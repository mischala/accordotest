# AccordoTest #

Mischa MacLeod's attempt at the Accordo coding test.
Designed to be as Vanilla ES6 as possible, running in a browser environment.
No client-side dependencies are used.
Tests are written in Mocha, which has issues with ES6, so babel is used to bridge the gap.

### How do I Run? ###

1. Clone the repo.
2. Open `src/view/index.html` in your favourite browser

### How do I build ###

1. Clone the repo.
2. `npm install`
3. `npm i -g browserify`
4. `npm run-script build` (This kicks off Babel and Browserify to make the bundle that the Browser can use.)
5. `npm test` (just to make sure.)
6. Open `src/view/index.html` in your favourite browser


### Contact? ###

Mischa.MacLeod@Gmail.com
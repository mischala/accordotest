'use strict';

let Console = "";

export function print(string){
    Console += `\n${string}`;
}

export function getConsole(){
    return Console;
}
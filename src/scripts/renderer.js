import * as Console from "./console.js";

export function render(bus) {
    console.debug("Trying to render");
    let consoleViewElement = document.getElementById("consoleView");
    consoleViewElement.value = Console.getConsole();
    consoleViewElement.scrollTop = consoleViewElement.scrollHeight;


    let oldBusElement = document.getElementById("bus");
    if(oldBusElement) {
        oldBusElement.remove();
    }

    if(bus){
        let busElement = document.createElement("div");
        busElement.setAttribute("class", "bus");
        busElement.setAttribute("id", "bus");

        let arrow = document.createTextNode("▲");
        busElement.appendChild(arrow);
        
        busElement.className += ` ${bus.facing}`;

        let busCell = document.getElementById(`r${bus.row}col${bus.col}`);
        busCell.appendChild(busElement);
    }

}
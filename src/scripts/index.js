import * as commands from "./commands.js";
import * as Console from "./console.js";
import * as Renderer from "./renderer.js";
export let bus;

function main() {
    Console.print("please enter a valid Place command");
    Renderer.render(bus);
}

export function onConsoleKeypress(){
    if(window.event.keyCode === 13){
        //get our input
        let input = document.getElementById("consoleInput").value;
        //clear the input bar
        document.getElementById("consoleInput").value = "";
        //parse input
        this.bus = parseAndExecCommand(input, this.bus);
        Renderer.render(this.bus);
    }
    return;
}

function parseAndExecCommand(input, bus) {
    let sanitizedInput = input.replace(/(?:\r\n|\r|\n)/g, '');
    let commaRemovedInput = sanitizedInput.replace(/,/g, ' ');
    let words = commaRemovedInput.split(" ", 4);

    switch(words[0]){
        case "place": //intentional fallthrough
        case "PLACE":
            bus = commands.place(parseInt(words[1]), parseInt(words[2]), words[3]);
            Console.print(`bus placed at column: ${bus.col} row: ${bus.row} facing: ${bus.facing}`);
            break;
        case "move": //intentional fallthrough
        case "MOVE":
            bus = commands.move(bus);
            Console.print("bus moved");
            Console.print(commands.report(bus));
            break;
        case "left": //intentional fallthrough
        case "LEFT":
            bus = commands.turn(bus, "LEFT");
            Console.print("bus turned left")
            Console.print(commands.report(bus));
            break;
        case "right": //intentional fallthrough
        case "RIGHT":
            bus = commands.turn(bus, "RIGHT");
            Console.print("bus turned right")
            Console.print(commands.report(bus));
            break;
        case "report": //intentional fallthrough
        case "REPORT":
            Console.print(commands.report(bus));
            break;
        default:
            Console.print("Invalid Command");
            break;
    }
    return bus;
}

window.onload = () => { main(); };
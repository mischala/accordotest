'use strict';

export function place(col, row, facing) {
    if(typeof row != "number" || typeof col != "number"){
        console.debug("specified row or colum is not a number, please choose numbers between 0 and 4");
        return null;
    }
    if(row < 0 || row > 4 || col < 0 || col > 4){
        console.debug("cannot place bus off the board, please choose numbers between 0 and 4");
        return null;
    }
    switch (facing) {
        case "north":
            facing = "NORTH";
            break;
        case "south":
            facing = "SOUTH";
            break;
        case "east":
            facing = "EAST";
            break;
        case "west":
            facing = "WEST";
            break;
        case "NORTH":
            break;
        case "SOUTH":
            break;
        case "EAST":
            break;
        case "WEST":
            break;
        default:
            console.debug("Invalid Facing, must be NORTH, SOUTH, EAST or WEST");
            return null;
    }

    let bus = {row, col, facing};
    console.debug(`bus placed at column: ${col}, row: ${row}, facing: ${facing}`);
    return bus;
}

export function move(bus) {
    let newBus = Object.assign({}, bus);
    if(!bus) {
        console.debug("cannot move a bus that isn't placed");
        return null;
     }
     switch (bus.facing) {
        case "NORTH":
            newBus.row = ++newBus.row;
            break;
        case "SOUTH":
            newBus.row = --newBus.row;
            break;
        case "EAST":
            newBus.col = ++newBus.col;
            break;
        case "WEST":
            newBus.col = --newBus.col;
            break;
    }

    if(testPositionOnBoard(newBus.row, newBus.col) == false ) {
        console.debug("cannot move bus off the board");
        return bus;
    } else {
        return newBus;
    }

}

export function turn(bus, direction) {
    let newBus = Object.assign({}, bus);
    if(direction != "LEFT" && direction != "RIGHT" && direction != "left" && direction != "right" ) {
        console.debug("Invalid direction, please enter either 'LEFT' or 'RIGHT'");
        return bus;
    }
    if(direction == "LEFT" || direction == "left") {
        switch(bus.facing) {
            case "NORTH":
                newBus.facing = "WEST";
                break;
            case "SOUTH":
                newBus.facing = "EAST";
                break;
            case "EAST":
                newBus.facing = "NORTH";
                break;
            case "WEST":
                newBus.facing = "SOUTH";
        }
    } else {
        switch(bus.facing) {
            case "NORTH":
                newBus.facing = "EAST";
                break;
            case "SOUTH":
                newBus.facing = "WEST";
                break;
            case "EAST":
                newBus.facing = "SOUTH";
                break;
            case "WEST":
                newBus.facing = "NORTH";
        }
    }
    return newBus;
}

export function report(bus) {
    if (!bus) {
        return "Bus has not been placed"
    }
    return `Bus is at column: ${bus.col}, row: ${bus.row}, facing: ${bus.facing}`;
}

function testPositionOnBoard(row, col) {
    if(row < 0 || row > 4 || col < 0 || col > 4){
        return false;
    }
    else {
        return true;
    }
}
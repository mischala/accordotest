var chai = require('chai');
var report = require('../src/scripts/commands.js').report;

describe('Report command tests', () => {
    it('will report the state of the bus', () => {
        var bus = {
            row: 2,
            col: 3,
            facing: "EAST"
        }
        var reportString = report(bus);
        chai.assert.equal(reportString, 
        "Bus is at column: 3, row: 2, facing: EAST");
    });
    it('will complain when the bus does not exist', () => {
        var bus = null;
        var reportString = report(bus);
        chai.assert.equal(reportString, 
        "Bus has not been placed");
    });
});
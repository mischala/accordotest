var chai = require('chai');
var move = require('../src/scripts/commands.js').move;

describe('Move command tests', () => {
    it('can move the bus to a valid position', () => {
        var bus = {
            row: 2,
            col: 2,
            facing: "NORTH"
        };
        bus = move(bus);
        chai.assert.equal(bus.row, 3);
    });

    it('cannot more the bus to an invalid position', () => {
        var bus = {
            row: 4,
            col: 4,
            facing: "NORTH" 
        };
        bus = move(bus);
        chai.assert.equal(bus.row, 4);
    });
});

var chai = require('chai');
var place = require('../src/scripts/commands.js').place;

describe('Place command tests', () => {
    it('can place the bus in a valid location and facing', () => {
        var bus = place(2,2,"NORTH");
        chai.assert.isOk(bus);
        chai.assert.equal(bus.row, 2);
        chai.assert.equal(bus.col, 2);
        chai.assert.equal(bus.facing, "NORTH");
    });

    it('cannot place a bus in an invalid location', () => {
        var bus = place(6,7,"NORTH");
        chai.assert.isNotOk(bus);
        bus = place(-5,10);
        chai.assert.isNotOk(bus);
    });

    it('cannot place the bus on an invalid facing', () => {
        var bus = place(2,2,"PENGUIN");
        chai.assert.isNotOk(bus);
    })
});
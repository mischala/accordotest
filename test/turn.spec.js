var chai = require('chai');
var turn = require('../src/scripts/commands.js').turn;

describe('Turn command tests', () => {
    it('can turn left', () => {
        var bus = {
            facing: "NORTH"
        }
        bus = turn(bus, "LEFT");
        chai.assert.equal(bus.facing, "WEST");
    });

    it('can turn right', () => {
        var bus = {
            facing: "NORTH"
        }
        bus = turn(bus, "RIGHT");
        chai.assert.equal(bus.facing, "EAST");
    });

    it('invalid direction does nothing', () => {
        var bus = {
            facing: "NORTH"
        }
        bus = turn(bus, "PENGUIN");
        chai.assert.equal(bus.facing, "NORTH");
    });
});
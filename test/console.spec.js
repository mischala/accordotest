var chai = require('chai');
var Console = require('../src/scripts/console.js');

describe('Console tests', () => {
    it('can add a string', () => {
        Console.print("string");

        chai.assert.equal(Console.getConsole(), `\nstring`)
    });
});
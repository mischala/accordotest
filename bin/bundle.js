(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.carpark = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.place = place;
exports.move = move;
exports.turn = turn;
exports.report = report;
function place(col, row, facing) {
    if (typeof row != "number" || typeof col != "number") {
        console.debug("specified row or colum is not a number, please choose numbers between 0 and 4");
        return null;
    }
    if (row < 0 || row > 4 || col < 0 || col > 4) {
        console.debug("cannot place bus off the board, please choose numbers between 0 and 4");
        return null;
    }
    switch (facing) {
        case "north":
            facing = "NORTH";
            break;
        case "south":
            facing = "SOUTH";
            break;
        case "east":
            facing = "EAST";
            break;
        case "west":
            facing = "WEST";
            break;
        case "NORTH":
            break;
        case "SOUTH":
            break;
        case "EAST":
            break;
        case "WEST":
            break;
        default:
            console.debug("Invalid Facing, must be NORTH, SOUTH, EAST or WEST");
            return null;
    }

    var bus = { row: row, col: col, facing: facing };
    console.debug("bus placed at column: " + col + ", row: " + row + ", facing: " + facing);
    return bus;
}

function move(bus) {
    var newBus = Object.assign({}, bus);
    if (!bus) {
        console.debug("cannot move a bus that isn't placed");
        return null;
    }
    switch (bus.facing) {
        case "NORTH":
            newBus.row = ++newBus.row;
            break;
        case "SOUTH":
            newBus.row = --newBus.row;
            break;
        case "EAST":
            newBus.col = ++newBus.col;
            break;
        case "WEST":
            newBus.col = --newBus.col;
            break;
    }

    if (testPositionOnBoard(newBus.row, newBus.col) == false) {
        console.debug("cannot move bus off the board");
        return bus;
    } else {
        return newBus;
    }
}

function turn(bus, direction) {
    var newBus = Object.assign({}, bus);
    if (direction != "LEFT" && direction != "RIGHT" && direction != "left" && direction != "right") {
        console.debug("Invalid direction, please enter either 'LEFT' or 'RIGHT'");
        return bus;
    }
    if (direction == "LEFT" || direction == "left") {
        switch (bus.facing) {
            case "NORTH":
                newBus.facing = "WEST";
                break;
            case "SOUTH":
                newBus.facing = "EAST";
                break;
            case "EAST":
                newBus.facing = "NORTH";
                break;
            case "WEST":
                newBus.facing = "SOUTH";
        }
    } else {
        switch (bus.facing) {
            case "NORTH":
                newBus.facing = "EAST";
                break;
            case "SOUTH":
                newBus.facing = "WEST";
                break;
            case "EAST":
                newBus.facing = "SOUTH";
                break;
            case "WEST":
                newBus.facing = "NORTH";
        }
    }
    return newBus;
}

function report(bus) {
    if (!bus) {
        return "Bus has not been placed";
    }
    return "Bus is at column: " + bus.col + ", row: " + bus.row + ", facing: " + bus.facing;
}

function testPositionOnBoard(row, col) {
    if (row < 0 || row > 4 || col < 0 || col > 4) {
        return false;
    } else {
        return true;
    }
}

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.print = print;
exports.getConsole = getConsole;
var Console = "";

function print(string) {
    Console += "\n" + string;
}

function getConsole() {
    return Console;
}

},{}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.bus = undefined;
exports.onConsoleKeypress = onConsoleKeypress;

var _commands = require("./commands.js");

var commands = _interopRequireWildcard(_commands);

var _console = require("./console.js");

var Console = _interopRequireWildcard(_console);

var _renderer = require("./renderer.js");

var Renderer = _interopRequireWildcard(_renderer);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var bus = exports.bus = void 0;

function main() {
    Console.print("please enter a valid Place command");
    Renderer.render(bus);
}

function onConsoleKeypress() {
    if (window.event.keyCode === 13) {
        //get our input
        var input = document.getElementById("consoleInput").value;
        //clear the input bar
        document.getElementById("consoleInput").value = "";
        //parse input
        this.bus = parseAndExecCommand(input, this.bus);
        Renderer.render(this.bus);
    }
    return;
}

function parseAndExecCommand(input, bus) {
    var sanitizedInput = input.replace(/(?:\r\n|\r|\n)/g, '');
    var commaRemovedInput = sanitizedInput.replace(/,/g, ' ');
    var words = commaRemovedInput.split(" ", 4);

    switch (words[0]) {
        case "place": //intentional fallthrough
        case "PLACE":
            bus = commands.place(parseInt(words[1]), parseInt(words[2]), words[3]);
            Console.print("bus placed at column: " + bus.col + " row: " + bus.row + " facing: " + bus.facing);
            break;
        case "move": //intentional fallthrough
        case "MOVE":
            bus = commands.move(bus);
            Console.print("bus moved");
            Console.print(commands.report(bus));
            break;
        case "left": //intentional fallthrough
        case "LEFT":
            bus = commands.turn(bus, "LEFT");
            Console.print("bus turned left");
            Console.print(commands.report(bus));
            break;
        case "right": //intentional fallthrough
        case "RIGHT":
            bus = commands.turn(bus, "RIGHT");
            Console.print("bus turned right");
            Console.print(commands.report(bus));
            break;
        case "report": //intentional fallthrough
        case "REPORT":
            Console.print(commands.report(bus));
            break;
        default:
            Console.print("Invalid Command");
            break;
    }
    return bus;
}

window.onload = function () {
    main();
};

},{"./commands.js":1,"./console.js":2,"./renderer.js":4}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.render = render;

var _console = require("./console.js");

var Console = _interopRequireWildcard(_console);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function render(bus) {
    console.debug("Trying to render");
    var consoleViewElement = document.getElementById("consoleView");
    consoleViewElement.value = Console.getConsole();
    consoleViewElement.scrollTop = consoleViewElement.scrollHeight;

    var oldBusElement = document.getElementById("bus");
    if (oldBusElement) {
        oldBusElement.remove();
    }

    if (bus) {
        var busElement = document.createElement("div");
        busElement.setAttribute("class", "bus");
        busElement.setAttribute("id", "bus");

        var arrow = document.createTextNode("▲");
        busElement.appendChild(arrow);

        busElement.className += " " + bus.facing;

        var busCell = document.getElementById("r" + bus.row + "col" + bus.col);
        busCell.appendChild(busElement);
    }
}

},{"./console.js":2}]},{},[3])(3)
});